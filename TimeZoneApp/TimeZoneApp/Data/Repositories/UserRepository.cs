﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TimeZoneApp.Models;

namespace TimeZoneApp.Data
{
    class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository() : base()
        {
        }

        public async Task<User> GetUserAsync(string login, string password)
        {
            return await connection.Table<User>().Where(i => (i.Login == login) && (i.Password == password)).FirstOrDefaultAsync();
        }

        public async Task<bool> IsLoginExistAsync(string login)
        {
            User user = await connection.Table<User>().Where(i => i.Login == login).FirstOrDefaultAsync();
            return user != null;
        }
    }
}
