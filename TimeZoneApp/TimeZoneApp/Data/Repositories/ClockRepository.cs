﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using SQLite;
using TimeZoneApp.Models;

namespace TimeZoneApp.Data
{
    class ClockRepository : GenericRepository<Clock>, IClockRepository
    {
        public ClockRepository() : base()
        {
        }

        public async Task<List<Clock>> GetClocksByUserIdAsync(int userId)
        {
            return await connection.Table<Clock>().Where(i => i.UserRef == userId).ToListAsync();
        }
    }
}