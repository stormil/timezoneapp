﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TimeZoneApp.Models;

namespace TimeZoneApp.Data
{
    public interface IGenericRepository<T> where T : class, IModel, new()
    {
        Task<T> GetItemAsync(int id);
        Task<int> SaveItemAsync(T item);
        Task<int> DeleteItemAsync(T item);
    }
}
