﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TimeZoneApp.Models;

namespace TimeZoneApp.Data
{
    public interface IClockRepository : IGenericRepository<Clock>
    {
        Task<List<Clock>> GetClocksByUserIdAsync(int userId);
    }
}
