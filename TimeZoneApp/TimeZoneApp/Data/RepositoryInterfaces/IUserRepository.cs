﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TimeZoneApp.Models;

namespace TimeZoneApp.Data
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<User> GetUserAsync(string login, string password);
        Task<bool> IsLoginExistAsync(string login);
    }
}
