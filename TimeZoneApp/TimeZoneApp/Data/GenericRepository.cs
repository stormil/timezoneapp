﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TimeZoneApp.Models;

namespace TimeZoneApp.Data
{
    class GenericRepository<T> : IGenericRepository<T> where T : class, IModel, new()
    {
        const string databasePath = "TimeZoneApp.db3";
        protected readonly SQLiteAsyncConnection connection;

        public GenericRepository()
        {
            connection = new SQLiteAsyncConnection(
                Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), databasePath));
            connection.CreateTableAsync<Clock>().Wait();
            connection.CreateTableAsync<User>().Wait();
        }

        public async Task<T> GetItemAsync(int id)
        {
            return await connection.Table<T>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<int> SaveItemAsync(T item)
        {
            if (item.Id != 0)
            {
                return await connection.UpdateAsync(item);
            }
            else
            {
                return await connection.InsertAsync(item);
            }
        }

        public async Task<int> DeleteItemAsync(T item)
        {
            return await connection.DeleteAsync(item);
        }
    }
}
