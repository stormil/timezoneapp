﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using TimeZoneApp.Data;
using TimeZoneApp.Models;
using TimeZoneApp.Services;
using Xamarin.Forms;

namespace TimeZoneApp.ViewModels
{
    public class ClockPageViewModel : ViewModelBase
    {
        // services
        readonly IClockRepository clockRepository;
        readonly ITimeGetterService timeGetterService;

        ClockListPageViewModel listViewModel;
        Clock clock;
        // tells the type of page 
        bool isAddNewPage;

        public ClockPageViewModel(
            INavigationService navigationService,
            ITimeGetterService timeGetterService,
            IClockRepository clockRepository)
            : base(navigationService)
        {
            Title = "Clock Page";

            this.clockRepository = clockRepository;
            this.timeGetterService = timeGetterService;
        }


        public ClockListPageViewModel ListViewModel
        {
            get { return listViewModel; }
            set { SetProperty(ref listViewModel, value); }
        }

        public Clock Clock
        {
            get { return clock; }
            set { SetProperty(ref clock, value); }
        }

        public bool IsAddNewPage
        {
            get { return isAddNewPage; }
            set { SetProperty(ref isAddNewPage, value); }
        }

        public bool IsInputValid()
        {
            if (string.IsNullOrEmpty(clock.FaceColor) || string.IsNullOrEmpty(clock.HandColor)
                || string.IsNullOrEmpty(clock.TimeZone))
                return false;
            return true;
        }

        private async void SaveData()
        {
            await clockRepository.SaveItemAsync(Clock);
        }

        // returns local time of the Clock TimeZone
        public DateTime GetTime()
        {
            if (string.IsNullOrEmpty(Clock.TimeZone))
            {
                return DateTime.Now;
            }
            return timeGetterService.GetDateTimeOfTimeZone(Clock.TimeZone);
        }


        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            IsAddNewPage = (bool)parameters["isAddNewPage"];
            if (parameters.ContainsKey("clockListPage")) ListViewModel = (ClockListPageViewModel)parameters["clockListPage"];
            Clock = (Clock)parameters["clock"] ?? new Clock();
            Clock.UserRef = listViewModel.User.Id;
        }

        public override void OnNavigatedFrom(NavigationParameters parameters)
        {
            if ((!isAddNewPage) && IsInputValid()) SaveData();
        }
    }
}
