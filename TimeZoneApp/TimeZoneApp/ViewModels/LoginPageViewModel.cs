﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using TimeZoneApp.Data;
using TimeZoneApp.Models;
using TimeZoneApp.Services;
using Xamarin.Forms;

namespace TimeZoneApp.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        readonly IAutoLoginisationService autoLoginisationService;
        readonly IUserRepository userRepository;

        private string login;
        private string password;
        private string log;


        public LoginPageViewModel(
            INavigationService navigationService,
            IUserRepository userRepository,
            IAutoLoginisationService autoLoginisationService)
            : base(navigationService)
        {
            Title = "Login Page";

            this.autoLoginisationService = autoLoginisationService;
            this.userRepository = userRepository;

            LogInMockUserCommand = new Command(LogInMockUser);
            CreateNewUserCommand = new Command(CreateNewUser);
            LogInCommand = new Command(LogInUser);

            LogInSavedUser();
        }


        public string Login
        {
            get { return login; }
            set { SetProperty(ref login, value); }
        }
        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }
        public string Log
        {
            get { return log; }
            set { SetProperty(ref log, value); }
        }

        public ICommand LogInMockUserCommand { get; private set; }
        public ICommand CreateNewUserCommand { get; private set; }
        public ICommand LogInCommand { get; private set; }

        // registering mock account before logging if it doesn't exists
        private async void LogInMockUser()
        {
            login = "test";
            password = "test";
            User user = await userRepository.GetUserAsync(Login, Password);
            if (user == null)
            {
                await userRepository.SaveItemAsync(new User()
                {
                    Login = Login,
                    Password = Password
                });
                user = await userRepository.GetUserAsync(Login, Password);
            }
            NavigateToClockList(user);
        }

        private async void LogInSavedUser()
        {
            login = await autoLoginisationService.GetLogin();
            password = await autoLoginisationService.GetPassword();
            if (login != null && password != null)
                LogInUser();
        }

        private async void CreateNewUser()
        {
            await NavigationService.NavigateAsync("RegistrationPage");
        }

        private async void LogInUser()
        {
            if (IsInputValid())
            {
                User user = await userRepository.GetUserAsync(Login, Password);
                if (user == null)
                {
                    Log = "Wrong login or password";
                    return;
                }
                NavigateToClockList(user);
            }
        }

        private bool IsInputValid()
        {
            if (string.IsNullOrEmpty(Login))
            {
                Log = "You must enter a login";
                return false;
            }
            if (string.IsNullOrEmpty(Password))
            {
                Log = "You must enter a password";
                return false;
            }
            return true;
        }

        private async void NavigateToClockList(User user)
        {
            var navigationParams = new NavigationParameters
                    {
                        { "user", user }
                    };
            autoLoginisationService.SaveUser(login, password);
            Login = "";
            Password = "";
            await NavigationService.NavigateAsync("ClockListPage", navigationParams);
        }
    }
}
