﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using TimeZoneApp.Data;
using TimeZoneApp.Models;
using TimeZoneApp.Services;
using TimeZoneApp.Views;
using Xamarin.Forms;

namespace TimeZoneApp.ViewModels
{
    public class ClockListPageViewModel : ViewModelBase
    {
        readonly ITimeGetterService timeGetterService;
        readonly IClockRepository clockRepository;
        ClockPageViewModel selectedClock;

        public ClockListPageViewModel(
            INavigationService navigationService,
            ITimeGetterService timeGetterService,
            IClockRepository clockRepository,
            ICanvasDrawerSevice canvasDrawerService)
            : base(navigationService)
        {
            Title = "Clock List Page";

            this.clockRepository = clockRepository;
            this.timeGetterService = timeGetterService;
            this.CanvasDrawerSevice = canvasDrawerService;

            Clocks = new ObservableCollection<ClockPageViewModel>();
            CreateClockCommand = new Command(CreateClock);
            DeleteClockCommand = new Command(DeleteClock);
            SaveClockCommand = new Command(SaveClock);
        }

        public ICanvasDrawerSevice CanvasDrawerSevice { get; set; }
        public ObservableCollection<ClockPageViewModel> Clocks { get; set; }
        public User User { get; set; }

        public ICommand CreateClockCommand { protected set; get; }
        public ICommand DeleteClockCommand { protected set; get; }
        public ICommand SaveClockCommand { protected set; get; }


        public ClockPageViewModel SelectedClock
        {
            get { return selectedClock; }
            set
            {
                if (selectedClock != value)
                {
                    SetProperty(ref selectedClock, value);
                    selectedClock = null;
                    var navigationParams = new NavigationParameters
                    {
                        { "clock", value.Clock },
                        { "clockListPage", this },
                        { "isAddNewPage", false }
                    };
                    NavigationService.NavigateAsync("ClockPage", navigationParams);
                }
            }
        }

        // loading list of clocks from database
        private async void GetData()
        {
            var clockList = await clockRepository.GetClocksByUserIdAsync(User.Id);
            foreach (Clock clock in clockList)
            {
                var temp = new ClockPageViewModel(NavigationService, timeGetterService, clockRepository)
                {
                    Clock = clock,
                    ListViewModel = this
                };
                Clocks.Add(temp);
            }
        }

        private async void CreateClock()
        {
            var navigationParams = new NavigationParameters
                {
                    { "clockListPage", this },
                    { "isAddNewPage", true }
                };
            await NavigationService.NavigateAsync("ClockPage", navigationParams);
        }

        // adding new clock to listview and database
        private async void SaveClock(object clockObject)
        {
            if (clockObject is ClockPageViewModel clock)
            {
                if (!clock.IsInputValid()) return;
                Clocks.Add(clock);
                await clockRepository.SaveItemAsync(clock.Clock);
            }
            await NavigationService.GoBackAsync();
        }

        // removing clock from listview and database
        private async void DeleteClock(object clockObject)
        {
            if (clockObject is ClockPageViewModel clock)
            {
                Clocks.Remove(clock);
                await clockRepository.DeleteItemAsync(clock.Clock);
            }
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            User = (User)parameters["user"];
            GetData();
        }
    }
}
