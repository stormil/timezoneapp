﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeZoneApp.Data;
using TimeZoneApp.Models;
using Xamarin.Forms;

namespace TimeZoneApp.ViewModels
{
    public class RegistrationPageViewModel : ViewModelBase
    {
        private string log;
        readonly IUserRepository userRepository;
        
        public RegistrationPageViewModel(
            INavigationService navigationService,
            IUserRepository userRepository)
            : base(navigationService)
        {
            Title = "Registration Page";
            this.userRepository = userRepository;
            RegisterCommand = new Command(RegisterUser);
        }

        public ICommand RegisterCommand { get; private set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public string Log
        {
            get
            {
                return log;
            }
            set
            {
                SetProperty(ref log, value);
            }
        }

        private async void RegisterUser()
        {
            if (await IsInputValid())
            {
                await userRepository.SaveItemAsync(new User
                {
                    Login = Login,
                    Password = Password
                });
                await NavigationService.GoBackAsync();
            }
        }

        private async Task<bool> IsInputValid()
        {
            if (string.IsNullOrEmpty(Login))
            {
                Log = "You must enter a login";
                return false;
            }
            if (string.IsNullOrEmpty(Password))
            {
                Log = "You must enter a password";
                return false;
            }
            if (await userRepository.IsLoginExistAsync(Login))
            {
                Log = "Login already exist";
                return false;
            }
            return true;
        }
    }
}
