﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using TimeZoneApp.ViewModels;
using Xamarin.Forms;

namespace TimeZoneApp.Views
{
    public partial class ClockPage : ContentPage
    {
        public ClockPage()
        {
            InitializeComponent();

            FillPickers();

            // refresh canvas timer
            Device.StartTimer(TimeSpan.FromSeconds(1f / 2), () =>
            {
                canvasView.InvalidateSurface();
                return true;
            }
            );
        }

        private void FillPickers()
        {
            foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                pickerTimeZone.Items.Add(z.Id);

            // populate picker with available colors
            foreach (string colorName in colorDictionary.Keys)
            {
                pickerFaceColor.Items.Add(colorName);
                pickerHandColor.Items.Add(colorName);
            }
        }

        private void OnPainting(object sender, SKPaintSurfaceEventArgs e)
        {
            // getting model data
            if (!(BindingContext is ClockPageViewModel clockPageViewModel)) return;
            var surface = e.Surface;
            var canvas = surface.Canvas;
            SKColor.TryParse(clockPageViewModel.Clock.HandColor, out SKColor handColor);
            SKColor.TryParse(clockPageViewModel.Clock.FaceColor, out SKColor faceColor);

            // drawing canvas
            clockPageViewModel.ListViewModel.CanvasDrawerSevice.
                PaintCanvas(canvas, handColor, faceColor, 
                clockPageViewModel.GetTime(), e.Info.Width, e.Info.Height);
        }

        private void PickerHandColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!(BindingContext is ClockPageViewModel clockPageViewModel)) return;
            clockPageViewModel.Clock.HandColor = colorDictionary[(string)pickerHandColor.SelectedItem];
        }

        private void PickerFaceColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!(BindingContext is ClockPageViewModel clockPageViewModel)) return;
            clockPageViewModel.Clock.FaceColor = colorDictionary[(string)pickerFaceColor.SelectedItem];
        }

        // dictionary to get Color from color name.
        readonly private Dictionary<string, string> colorDictionary = new Dictionary<string, string>
        {
            { "Black", "#212121" },         { "Blue", "#2196F3" },
            { "Blue Grey", "#607D8B" },     { "Brown", "#795548" },
            { "Cyan", "#00BCD4" },          { "Dark Orange", "#FF5722" },
            { "Dark Purple", "#673AB7" },   { "Green", "#4CAF50" },
            { "Grey", "#9E9E9E" },          { "Indigo", "#3F51B5" },
            { "Light Blue", "#02A8F3" },    { "Light Green", "#8AC249" },
            { "Lime", "#CDDC39" },          { "Orange", "#FF9800" },
            { "Pink", "#E91E63" },          { "Purple", "#94499D" },
            { "Red", "#D32F2F" },           { "Teal", "#009587" },
            { "Amber", "#FFC107" },         { "Yellow", "#FFEB3B"},
        };
    }
}
