﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeZoneApp.Services;
using TimeZoneApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeZoneApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CanvasViewCell : ViewCell
    {
        public CanvasViewCell()
        {
            InitializeComponent();

            // Refresh canvas timer
            Device.StartTimer(TimeSpan.FromSeconds(1f / 2), () =>
            {
                canvasView.InvalidateSurface();
                return true;
            }
            );
        }

        private void OnPainting(object sender, SKPaintSurfaceEventArgs e)
        {
            // getting model data
            if (!(BindingContext is ClockPageViewModel clockPageViewModel)) return;
            var surface = e.Surface;
            var canvas = surface.Canvas;
            SKColor.TryParse(clockPageViewModel.Clock.HandColor, out SKColor handColor);
            SKColor.TryParse(clockPageViewModel.Clock.FaceColor, out SKColor faceColor);

            // drawing canvas
            clockPageViewModel.ListViewModel.CanvasDrawerSevice.
                PaintCanvas(canvas, handColor, faceColor,
                clockPageViewModel.GetTime(), e.Info.Width, e.Info.Height);
        }
    }
}