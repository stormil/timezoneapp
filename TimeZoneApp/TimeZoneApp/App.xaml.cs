﻿using Prism;
using Prism.Ioc;
using TimeZoneApp.ViewModels;
using TimeZoneApp.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.Unity;
using System.IO;
using System;
using Unity;
using TimeZoneApp.Services;
using TimeZoneApp.Data;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace TimeZoneApp
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync("NavigationPage/LoginPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            // registering pages
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<ClockListPage>();
            containerRegistry.RegisterForNavigation<ClockPage>();
            containerRegistry.RegisterForNavigation<LoginPage>();
            containerRegistry.RegisterForNavigation<RegistrationPage>();

            // registering services
            containerRegistry.Register<IAutoLoginisationService, AutoLoginisationService>();
            containerRegistry.Register<ITimeGetterService, TimeGetterService>();
            containerRegistry.RegisterSingleton<IClockRepository, ClockRepository>();
            containerRegistry.RegisterSingleton<IUserRepository, UserRepository>();
            containerRegistry.Register<ICanvasDrawerSevice, CanvasDrawerService>();
        }
    }
}
