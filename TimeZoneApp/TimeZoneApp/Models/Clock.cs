﻿using Prism.Mvvm;
using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace TimeZoneApp.Models
{
    public class Clock: BindableBase, IModel
    {
        private string timeZone;
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [NotNull]
        public string TimeZone
        {
            get
            {
                return timeZone;
            }
            set
            {
                SetProperty(ref timeZone, value);
            }
        }
        [NotNull]
        public string FaceColor { get; set; }
        [NotNull]
        public string HandColor { get; set; }

        [ForeignKey(typeof(User)), NotNull]
        public int UserRef { get; set; }
    }
}
