﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeZoneApp.Models
{
    public interface IModel
    {
        int Id { get; set; }
    }
}
