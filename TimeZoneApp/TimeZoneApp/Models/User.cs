﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace TimeZoneApp.Models
{
    public class User: IModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Unique]
        public string Login { get; set; }
        [NotNull]
        public string Password { get; set; }

        [OneToMany]
        public List<Clock> Clocks { get; set; }
    }
}
