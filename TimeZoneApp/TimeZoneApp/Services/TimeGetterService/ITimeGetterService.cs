﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeZoneApp.Services
{
    public interface ITimeGetterService
    {
        DateTime GetDateTimeOfTimeZone(string timezone);
    }
}
