﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeZoneApp.Services
{
    class TimeGetterService : ITimeGetterService
    {
        public DateTime GetDateTimeOfTimeZone(string timezone)
        {
            TimeZoneInfo estZone = TimeZoneInfo.FindSystemTimeZoneById(timezone);
            return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, estZone);
        }
    }
}
