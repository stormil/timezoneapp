﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TimeZoneApp.Models;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace TimeZoneApp.Services
{
    class AutoLoginisationService : IAutoLoginisationService
    {
        public async void SaveUser(string login, string password)
        {
            SecureStorage.RemoveAll();
            await SecureStorage.SetAsync("login", login);
            await SecureStorage.SetAsync("password", password);
        }

        public async Task<string> GetLogin()
        {
            return await SecureStorage.GetAsync("login");
        }

        public async Task<string> GetPassword()
        {
            return await SecureStorage.GetAsync("password");
        }
    }
}
