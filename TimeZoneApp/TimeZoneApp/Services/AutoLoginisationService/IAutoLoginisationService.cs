﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TimeZoneApp.Services
{
    public interface IAutoLoginisationService
    {
        void SaveUser(string login, string password);
        Task<string> GetLogin();
        Task<string> GetPassword();
    }
}
