﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Text;
using TimeZoneApp.ViewModels;

namespace TimeZoneApp.Services
{
    class CanvasDrawerService: ICanvasDrawerSevice
    {
        SKPaint handsPaint = new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = SKColors.White,
            StrokeWidth = 5,
            StrokeCap = SKStrokeCap.Round,
            IsAntialias = true
        };

        SKPaint facePaint = new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = SKColors.White,
            StrokeWidth = 1,
            StrokeCap = SKStrokeCap.Round,
            IsAntialias = true
        };

        public void PaintCanvas(SKCanvas canvas, SKColor handColor, SKColor faceColor, DateTime time, int width, int height)
        {   
            canvas.Clear(SKColors.White);
            
            handsPaint.Color = handColor;
            facePaint.Color = faceColor;

            // Set transforms
            canvas.Translate(width / 2, height / 2);
            canvas.Scale(Math.Min(width / 250f, height / 250f));

            DrawFace(canvas);
            DrawHands(canvas, time);
        }

        private void DrawFace(SKCanvas canvas)
        {
            // face circles
            facePaint.Style = SKPaintStyle.StrokeAndFill;
            for (int i = 0; i < 360; i += 6)
            {
                if (i % 90 != 0) canvas.DrawCircle(0, -90, i % 30 == 0 ? 2 : 0, facePaint);
                canvas.RotateDegrees(6);
            }
            facePaint.Style = SKPaintStyle.Stroke;

            // face digits
            canvas.DrawText("12", -7, -90, facePaint);
            canvas.DrawText("3", 90, 3, facePaint);
            canvas.DrawText("6", -3, 96, facePaint);
            canvas.DrawText("9", -95, 3, facePaint);
        }

        private void DrawHands(SKCanvas canvas, DateTime time)
        {
            // hours hand
            canvas.Save();
            canvas.RotateDegrees(30 * time.Hour + time.Minute / 2f);
            handsPaint.StrokeWidth = 15;
            canvas.DrawLine(0, 0, 0, -50, handsPaint);
            canvas.Restore();

            // minutes hand
            canvas.Save();
            canvas.RotateDegrees(6 * time.Minute + time.Second / 10f);
            handsPaint.StrokeWidth = 10;
            canvas.DrawLine(0, 0, 0, -70, handsPaint);
            canvas.Restore();

            // seconds hand
            canvas.Save();
            canvas.RotateDegrees(6 * (time.Second + time.Millisecond / 1000f));
            handsPaint.StrokeWidth = 2;
            canvas.DrawLine(0, 10, 0, -80, handsPaint);
            canvas.Restore();
        }
    }
}
