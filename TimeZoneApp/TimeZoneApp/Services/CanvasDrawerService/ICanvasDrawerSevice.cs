﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Text;
using TimeZoneApp.ViewModels;

namespace TimeZoneApp.Services
{
    public interface ICanvasDrawerSevice
    {
        void PaintCanvas(SKCanvas canvas, SKColor handColor,
            SKColor faceColor, DateTime time, int width, int height);
    }
}
